import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FileTAP } from '../class/file';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  URL:string = "http://localhost:8282/api/takeandput/file/"
  
  constructor() { }

  async getList(){
    let list:FileTAP[] = []
    const rep = await fetch(this.URL + "list").then(reponse => reponse.json())
    .then(jsonData => list = jsonData);
    return list;
  }

  async downloadFile(id:string){
    document.location.href = this.URL + id + "/download";
  }

  async uploadFile(files:Blob[]){
    const formData = new FormData();
    for(const file of files){
      formData.append('file', file);
    }
    await fetch(this.URL + "upload", {
      method: "POST",
      body: formData
    }).then((res) => console.log(res))
  }
}
