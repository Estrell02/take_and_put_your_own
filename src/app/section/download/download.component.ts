import { Component, OnInit } from '@angular/core';
import { FileTAP } from 'src/app/class/file';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  isNotDone:boolean = true
  filesTap:FileTAP[] = []
  Math = Math

  constructor(private api:ApiService) { }

  async ngOnInit() {
    await this.api.getList().then((val) => {
      this.filesTap = val;
      this.isNotDone = false;
    });
  }

  onDownload(id:string){
    this.api.downloadFile(id);
  }

}
