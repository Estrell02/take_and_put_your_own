import { Component, OnInit } from '@angular/core';
import { NgxFileDropEntry } from 'ngx-file-drop/ngx-file-drop/ngx-file-drop-entry';
import { FileTAP } from 'src/app/class/file';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  public files:NgxFileDropEntry[] = [];
  uploadFiles:FileTAP[] = [];
  Math = Math

  constructor(private api:ApiService) { }

  ngOnInit(): void {
  }

  async dropped(files:NgxFileDropEntry[]){
    for(const droppedFile of files){
      if(droppedFile.fileEntry.isFile){
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        let sends:Blob[] = []
        fileEntry.file((file:File) => {
          let fileTap = new FileTAP(file.name, file.type, file.size);
          this.uploadFiles.push(fileTap)
          sends.push(file);
          console.log(droppedFile.relativePath, file);
        })
        this.api.uploadFile(sends);
      }else{
        const fileEntry =droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  fileOver(event:any){
    console.log(event);
  }

  fileLeave(event:any){
    console.log(event);
  }
}
